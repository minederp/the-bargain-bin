package me.KeybordPiano459.TheBargainBin.events;

import me.KeybordPiano459.TheBargainBin.AuctionData.Bid;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class AuctionBidEvent extends Event {

    private Bid bid;
    private static final HandlerList handlers = new HandlerList();

    public AuctionBidEvent(Bid bid) {
        this.bid = bid;
    }

    public Bid getBid() {
        return this.bid;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}