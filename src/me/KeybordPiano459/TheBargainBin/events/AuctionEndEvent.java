package me.KeybordPiano459.TheBargainBin.events;

import me.KeybordPiano459.TheBargainBin.AuctionData.Auction;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class AuctionEndEvent extends Event {

    private Auction auction;
    private static final HandlerList handlers = new HandlerList();

    public AuctionEndEvent(Auction auction) {
        this.auction = auction;
    }

    public Auction getAuction() {
        return this.auction;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}