package me.KeybordPiano459.TheBargainBin.events;

import me.KeybordPiano459.TheBargainBin.AuctionController;
import me.KeybordPiano459.TheBargainBin.TheBargainBin;

public class AuctionTimeTask implements Runnable {
    TheBargainBin plugin;
    public AuctionTimeTask(TheBargainBin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        AuctionTimeEvent event = new AuctionTimeEvent(AuctionController.getAuction());
        plugin.getServer().getPluginManager().callEvent(event);
    }
}