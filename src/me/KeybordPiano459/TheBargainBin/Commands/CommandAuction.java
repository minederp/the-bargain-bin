package me.KeybordPiano459.TheBargainBin.Commands;

import Utilities.Helper;
import me.KeybordPiano459.TheBargainBin.AuctionData.Auction;
import me.KeybordPiano459.TheBargainBin.AuctionController;
import me.KeybordPiano459.TheBargainBin.AuctionData.AuctionItem;
import me.KeybordPiano459.TheBargainBin.TheBargainBin;
import me.KeybordPiano459.TheBargainBin.events.AuctionStartEvent;
import me.spathwalker.noteboard.NoteManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandAuction implements CommandExecutor {

	TheBargainBin plugin;

	public CommandAuction(TheBargainBin plugin) {
		this.plugin = plugin;
	}
	private String prefix = "[" + ChatColor.BLUE + "The Bargain Bin" + ChatColor.RESET + "] ";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		switch (args.length) {
			case 0:
				sendPlayerHelp(sender);
				break;
			case 1:
				String subCommand = args[0];
				if (subCommand.equalsIgnoreCase("help")) {
					sendPlayerHelp(sender);
				} else if (subCommand.equalsIgnoreCase("info")) {
					sendPlayerInfo(sender);
				} else if (subCommand.equalsIgnoreCase("on")) {
					turnNotificationsOn(sender);
				} else if (subCommand.equalsIgnoreCase("off")) {
					turnNotificationsOff(sender);
				} else if ((sender.isOp() || sender.hasPermission("auction.stop")) && subCommand.equalsIgnoreCase("stop")){
					AuctionController.disable();
				} else {
					sendPlayerHelp(sender);
				}
				break;
			case 2:
				if (sender instanceof ConsoleCommandSender) {
					Helper.send(sender, "Consoles cannot start auctions.");
				}
				if(!startAuction(sender, args)){
					return false;
				}
				break;
		}
		return true;
	}

	private void sendPlayerHelp(CommandSender player) {
		player.sendMessage(ChatColor.GRAY + "---------- [" + ChatColor.BLUE + "The Bargain Bin" + ChatColor.GRAY + "] ----------");
		player.sendMessage(ChatColor.GRAY + " - " + ChatColor.GOLD + "/auction [help] - Displays all auction commands");
		player.sendMessage(ChatColor.GRAY + " - " + ChatColor.GOLD + "/auction [info] - Show info about current auction");
		player.sendMessage(ChatColor.GRAY + " - " + ChatColor.GOLD + "/auction [off] - Disable auction messages");
		player.sendMessage(ChatColor.GRAY + " - " + ChatColor.GOLD + "/auction [on] - Enable auction messages");
		player.sendMessage(ChatColor.GRAY + " - " + ChatColor.GOLD + "/auction [start] [amount] - Start an auction");
		player.sendMessage(ChatColor.GRAY + " - " + ChatColor.GOLD + "/bid [amount] - Bid on the current auction");
	}

	private void sendPlayerInfo(CommandSender sender) {
		if(AuctionController.getAuction() != null){
			String[] auctionInfo = AuctionController.getAuction().getAuctionInfo();
			for (String line : auctionInfo) {
				Helper.send(sender, line);
			}
		} else {
			Helper.newSend(sender, prefix, "No auctions currently.");
		}
	}

	private void turnNotificationsOn(CommandSender sender) {
		Helper.newSend(sender, prefix, "Auction messages on.");
		AuctionController.getPlayersWithNotifOff().remove(sender.getName());
	}

	private void turnNotificationsOff(CommandSender sender) {
		Helper.newSend(sender, prefix, "Auction messages off.");
		NoteManager.removeNote((Player)sender, AuctionController.getBoard());
		AuctionController.getPlayersWithNotifOff().add(sender.getName());
	}

	private boolean startAuction(CommandSender sender, String[] args) {
		Player player = (Player) sender;
		ItemStack item = player.getItemInHand();
		String amt = args[1];
		int amount = 0;
		try {
			amount = Integer.valueOf(amt);
			if (amount < 1) {
				player.sendMessage(prefix + "That isn't a valid starting amount!");
				return false;
			}
		} catch (NumberFormatException e) {
			player.sendMessage(prefix + "That isn't a valid starting amount!");
			return false;
		}
		if (item.getType() == Material.AIR) {
			player.sendMessage(prefix + "You need to be holding an item!");
			return false;
		}
		Auction auction = new Auction(player, amount, new AuctionItem(item));
		AuctionStartEvent auevent = new AuctionStartEvent(auction);
		Bukkit.getServer().getPluginManager().callEvent(auevent);
		return true;
	}
}