package me.KeybordPiano459.TheBargainBin;

import Utilities.Helper;
import me.KeybordPiano459.TheBargainBin.Commands.CommandBid;
import me.KeybordPiano459.TheBargainBin.Commands.CommandAuction;
import me.KeybordPiano459.TheBargainBin.events.AuctionTimeTask;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class TheBargainBin extends JavaPlugin {
    
	private static AuctionController controller;

    @Override
    public void onEnable() {
		log("Enabled");
		controller = new AuctionController(this);
		registerCommands();
		registerListeners();
		registerTasks();
		if(Utilities.Main.econHandler == null){
			log("Unable to initilize EconomyHanlder.  Shutting down.");
			Bukkit.getPluginManager().disablePlugin(this);
		}
    }

    @Override
    public void onDisable() {
		log("Disabled");
		AuctionController.disable();
    }
    
	private void registerCommands() {
		this.getCommand("auction").setExecutor(new CommandAuction(this));
        this.getCommand("bid").setExecutor(new CommandBid(this));
		this.getCommand("a").setExecutor(new CommandAuction(this));
	}

	private void registerListeners() {
		this.getServer().getPluginManager().registerEvents(controller, this);
	}

	private void registerTasks() {
		this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new AuctionTimeTask(this), 20, 20);
	}
	
	public static void log(String message){
		Helper.newLog(AuctionController.getPrefix(), message);
	}
}