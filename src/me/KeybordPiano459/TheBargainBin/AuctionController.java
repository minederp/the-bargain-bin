package me.KeybordPiano459.TheBargainBin;

import Utilities.Helper;
import me.KeybordPiano459.TheBargainBin.AuctionData.Bid;
import me.KeybordPiano459.TheBargainBin.AuctionData.Auction;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import me.KeybordPiano459.TheBargainBin.AuctionData.AuctionItem;
import me.KeybordPiano459.TheBargainBin.AuctionData.BaseItemType;
import me.KeybordPiano459.TheBargainBin.AuctionData.EnchantmentNames;
import me.KeybordPiano459.TheBargainBin.events.AuctionBidEvent;
import me.KeybordPiano459.TheBargainBin.events.AuctionEndEvent;
import me.KeybordPiano459.TheBargainBin.events.AuctionStartEvent;
import me.KeybordPiano459.TheBargainBin.events.AuctionTimeEvent;
import me.spathwalker.noteboard.NoteManager;
import me.spathwalker.noteboard.Noteboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;

public class AuctionController implements Listener {

	private static TheBargainBin plugin;
	private static final String prefix = "[" + ChatColor.BLUE + "TBB" + ChatColor.RESET + "] ";
	private static Noteboard board;
	private static Auction theAuction = null;
	private static List<String> playersWithNotifOff = new ArrayList<>();

	public static Auction getAuction() {
		return theAuction;
	}

	private static void broadcastAuctionInfo(String... strings) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (!playersWithNotifOff.contains(player.getName())) {
				for (String line : strings) {
					Helper.newSend(player, prefix, line);
				}
			}
		}
	}
	private float minimumBidPercent = 1;
	private float auctionFeePercent = 2;

	public AuctionController(TheBargainBin plugin) {
		AuctionController.plugin = plugin;
	}

	public static List<String> getPlayersWithNotifOff() {
		return playersWithNotifOff;
	}

	public static Noteboard getBoard() {
		return board;
	}

	public static String getPrefix() {
		return prefix;
	}

	public static TheBargainBin getPlugin() {
		return plugin;
	}

	public static Auction getTheauction() {
		return theAuction;
	}

	public String getEnchantmentName(Enchantment enchantment) {
		return EnchantmentNames.getName(enchantment.getName());
	}

	public static void disable() {
		if (theAuction != null) {
			Player player = theAuction.getAuctionStarter();
			ItemStack item = theAuction.getItem().getItem();
			processItemTransfer(player, theAuction.getItem());
			theAuction = null;
			for (Player playera : Bukkit.getOnlinePlayers()) {
				if (!getPlayersWithNotifOff().contains(playera.getName())) {
					NoteManager.removeNote(playera, board);
				}
			}
		}
	}

	@EventHandler
	public void onAuctionStart(AuctionStartEvent event) {
		Auction auction = event.getAuction();
		Player player = auction.getAuctionStarter();
		if (theAuction != null) {
			player.sendMessage(prefix + "There is already an auction in progress.");
		} else if (!Utilities.Main.econHandler.has(player.getName(), 10)) {
			player.sendMessage(prefix + "You can't afford the 10$ fee to start an auction.");
		} else {
			theAuction = auction;
			auction.setTime(60);
			player.getInventory().clear(player.getInventory().getHeldItemSlot());
			Helper.newSend(player, prefix, "You are charged 10$ flat fee for this auction.");
			Utilities.Main.econHandler.withdrawPlayer(player.getName(), 10);
			AuctionController.broadcastAuctionInfo("&C" + auction.getAuctionStarter().getName() + " has started an auction!");
			AuctionController.broadcastAuctionInfo(auction.getAuctionInfo());
			List<String> lines = getBoardText(auction);
			board = new Noteboard("BargainBin", "BB", true, auction.getTime() + 3600, lines.toArray(new String[lines.size()]));
			for (Player playera : Bukkit.getOnlinePlayers()) {
				if (!getPlayersWithNotifOff().contains(playera.getName())) {
					board.show(playera);
				}
			}
		}
	}

	@EventHandler
	public void onAuctionBid(AuctionBidEvent event) {
		Bid bid = event.getBid();
		Auction auction = bid.getAuction();
		Player player = bid.getPlayer();
		int amount = bid.getAmount();
		if (player.getName().equals(auction.getAuctionStarter().getName())) {
			player.sendMessage(prefix + "You can't bid on your own auction!");
			return;
		}
		if (Utilities.Main.econHandler.has(player.getName(), amount)) {
			float minimumBid = getMinimumBid(auction.getAmount(), auction.getBidder());
			if (minimumBid < auction.getAmount()) {
				minimumBid = auction.getAmount() + 10;
			}
			if (amount >= minimumBid) {
				auction.setWasBidOn(true);
				auction.setAmount(amount);
				auction.setBidder(player);
				if (auction.getTime() <= 15) {
					auction.setTime(15);
				}
				board.setSuffix(0, ": " + ChatColor.AQUA + "$" + amount);
				board.setSuffix(indexOfBidder, player.getName());
				broadcastAuctionInfo("&cBID: &b$" + bid.getAmount() + " &cby &6" + bid.getPlayer().getName());
			} else {
				Helper.newSend(player, prefix, "Your bid isn't high enough.  The minimum bid is $" + minimumBid);
			}
		} else {
			Helper.newSend(player, prefix, "You don't have enough money for this bid.");
		}
	}

	private float getMinimumBid(float amount, Player player) {
		if (player == null) {
			return amount;
		}
		if (amount * (this.minimumBidPercent / (float) 100) > 10) {
			return (amount * (this.minimumBidPercent / (float) 100) + amount);
		}
		return 10 + amount;
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onLogin(PlayerLoginEvent e) {
		Bukkit.getScheduler().runTaskLater(plugin, new AuctionJoinRunnable(e.getPlayer()), 5);
	}

	private class AuctionJoinRunnable implements Runnable {

		private Player player;

		public AuctionJoinRunnable(Player player) {
			this.player = player;
		}

		@Override
		public void run() {
			if (!playersWithNotifOff.contains(player.getName()) && getAuction() != null) {
				try {
					board.show(player);
				} catch (IllegalStateException ex) {
				}
			}
		}
	}

	@EventHandler
	public void onAuctionEnd(AuctionEndEvent event) {
		Auction auction = event.getAuction();
		Player seller = auction.getAuctionStarter();
		Player bidder = auction.getBidder();
		double fee = (auctionFeePercent / 100) * auction.getAmount();
		if (auctionBidValid(auction)) {
			if (!auction.wasBidOn()) {
				broadcastAuctionInfo("Nobody bid on " + auction.getItem().getItemName());
				processItemTransfer(seller, auction.getItem());
				theAuction = null;
			} else {
				broadcastAuctionInfo(bidder.getName() + " purchased a " + auction.getItem().getItemName() + " for $" + auction.getAmount());
				processTransaction(seller, bidder, auction.getAmount());
				processItemTransfer(bidder, auction.getItem());
				if (fee > 1) {
					Helper.newSend(seller, prefix, "You are charged a " + auctionFeePercent + "% fee on all auctions for $" + fee);
					Utilities.Main.econHandler.withdrawPlayer(seller.getName(), fee);
				}
				theAuction = null;
			}
		}
		theAuction = null;
		//set the board on the right to off.
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (!getPlayersWithNotifOff().contains(player.getName())) {
				NoteManager.removeNote(player, board);
			}
		}
		board.remove();
		board = null;
	}

	@EventHandler
	public void onAuctionTime(AuctionTimeEvent event) {
		Auction auction = event.getAuction();
		if (auction != null) {
			List<String> lines = getBoardText(auction);
			auction.setTime(auction.getTime() - 1);
			try {
				board.setSuffix(indexOfTimeString, (auction.getTime() - 1) + "s");
			} catch (NoSuchElementException e) {
				board.clearText();
				board.setText(getBoardText(auction).toArray(new String[0]));
			}

			if (auction.getTime() == 0) {
				AuctionEndEvent aeevent = new AuctionEndEvent(auction);
				plugin.getServer().getPluginManager().callEvent(aeevent);
			}
		}

	}

	private boolean auctionBidValid(Auction auction) {
		if (auction.getBidder() != null && !auction.getBidder().isOnline()) {
			broadcastAuctionInfo(ChatColor.RED + "The bidder has logged out.  Auction cancelled.  Abuse will be punished.");
			return false;
		}
		if (auction.wasBidOn()) {
			if (auction.getBidder() == null || !auction.getBidder().isOnline()) {
				broadcastAuctionInfo(ChatColor.RED + "The bidder has logged out.  Auction cancelled.  Abuse will be punished.");
				AuctionController.disable();
				return false;
			}
			if (!Utilities.Main.econHandler.has(auction.getBidder().getName(), auction.getAmount())) {
				broadcastAuctionInfo(ChatColor.RED + "The bidder no longer has funds for the auction.  Abuse is noted.");
				return false;
			}
		}
		return true;
	}

	private void processTransaction(Player seller, Player bidder, int amount) {
		Utilities.Main.econHandler.depositPlayer(seller.getName(), amount);
		Helper.newSend(seller, prefix, "&cYou have recieved &2$" + amount);
		Utilities.Main.econHandler.withdrawPlayer(bidder.getName(), amount);
		Helper.newSend(bidder, prefix, "&cYou have been debited &2$" + amount);
	}

	private static void processItemTransfer(Player bidder, AuctionItem item) {
		if (bidder != null && item != null) {
			bidder.sendMessage(prefix + "You have received " + item.getItem().getAmount() + " " + item.getItemName() + "(s)");
			if (bidder.getInventory().firstEmpty() == -1) {
				bidder.sendMessage(prefix + "Your inventory is full.  Item is at your feet.");
				bidder.getWorld().dropItemNaturally(bidder.getLocation(), item.getItem());
			} else {
				bidder.getInventory().addItem(item.getItem());
			}
		}
	}
	
	private int indexOfTimeString = 1;
	private int indexOfBidder = 2;

	private List<String> getBoardText(Auction auction) {
		List<String> lines = new ArrayList<>();
		String name;
		String item_details = ChatColor.RED + "" + auction.getItem().getItem().getAmount() + " * " + auction.getItem().getItemName();
		if (item_details.length() > 16) {
			if (item_details.length() > 32) {
				item_details = item_details.substring(0, 32);
			}
			int cutPoint = item_details.length() - 16;
			item_details = item_details.substring(0, cutPoint)
					+ "|" + item_details.substring(cutPoint, item_details.length());
		}
		lines.add(item_details + "|:" + " $" + auction.getAmount());
		String extraInfo = auction.getItem().getExtraInfo();
		if (extraInfo == null) {
			indexOfTimeString = 1;
			indexOfBidder = 2;
		} else {
			indexOfTimeString = 2;
			indexOfBidder = 3;
			lines.add(ChatColor.RED + "ExtraInfo: " + ChatColor.WHITE + auction.getItem().getExtraInfo());
		}
		lines.add(ChatColor.RED + "Time| Remaining: " + ChatColor.GREEN + "|" + (auction.getTime() - 1) + "s");
		lines.add(ChatColor.RED + "TopBidder: " + ChatColor.AQUA + "|" + ChatColor.GRAY + "None");
		lines.add(ChatColor.GOLD + "Enchantments: " + auction.getItem().getEnchantments().size());
		if (auction.getItem().getBaseType() != null && auction.getItem().getBaseType().equals(BaseItemType.TOOL)) {
			lines.add(ChatColor.GRAY + "Durability: " + ChatColor.WHITE + auction.getItem().getDurabiltyInfo());
		}
		if (auction.getItem().getLore() != null) {
			lines.add(ChatColor.GOLD + "Lores: " + auction.getItem().getLore().size());
		}
		return lines;
	}
	
/* No longer needed, setSuffix() used instead.
	private String getTimeString() {
		return (ChatColor.RED + "Time Remaining: " + ChatColor.GREEN + (getAuction().getTime() - 1) + "s");
	}
	
	private String getAuctionFirstLine() {
		Auction auction = getAuction();
		return (ChatColor.RED + "" + auction.getItem().getItem().getAmount() + " * " + auction.getItem().getItemName() + ":" + " $" + auction.getAmount());
	}
*/
}