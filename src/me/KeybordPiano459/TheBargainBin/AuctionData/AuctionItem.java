/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.KeybordPiano459.TheBargainBin.AuctionData;

import Utilities.Helper;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

/**
 *
 * @author charles
 */
public final class AuctionItem {

	private ItemStack item;
	private int ItemTypeID;
	private Material type;
	private BaseItemType baseType;
	private boolean hasLore = false;
	private boolean hasCustomName = false;
	private boolean hasEnchants = false;
	private String extraInfo;

	public AuctionItem(ItemStack item) {
		this.item = item;
		this.ItemTypeID = item.getTypeId();
		this.type = item.getType();
		this.baseType = BaseItemType.getBaseItemType(item);
		if (this.item.hasItemMeta()) {
			if (this.item.getItemMeta().hasLore()) {
				this.hasLore = true;
			}
			if (this.item.getItemMeta().hasDisplayName()) {
				this.hasCustomName = true;
			}
		}
		if (!this.item.getEnchantments().isEmpty()){
			this.hasEnchants = true;
		}
		this.extraInfo = BaseItemType.getExtendInfo(this);
	}

	public String getItemName() {
		String name = this.item.getType().toString().substring(0, 1).toUpperCase() + this.item.getType().toString().substring(1).toLowerCase().replaceAll("_", " ");
		return name;
	}

	public String getCustomName() {
		if (this.item.hasItemMeta()) {
			if (item.getItemMeta().getDisplayName() != null) {
				return ChatColor.AQUA + item.getItemMeta().getDisplayName() + ChatColor.RED;
			}
		}
		return null;
	}

	public ItemStack getItem() {
		return item;
	}

	public int getItemTypeID() {
		return ItemTypeID;
	}

	public Material getType() {
		return type;
	}

	public BaseItemType getBaseType() {
		return baseType;
	}

	public String getExtraInfo() {
		return extraInfo;
	}

	public boolean hasLore() {
		return hasLore;
	}

	public boolean hasCustomName() {
		return hasCustomName;
	}

	public boolean hasEnchants() {
		return hasEnchants;
	}

	public List<String> getLore() {
		return getItem().getItemMeta().getLore();
	}
	
	public Map<Enchantment, Integer> getEnchantments() {
		if(ItemTypeID == 403){
			return ((EnchantmentStorageMeta) item.getItemMeta()).getEnchants();
		}
		return getItem().getEnchantments();
	}

	public String getDurabiltyInfo() {
		int currentDurability = item.getType().getMaxDurability() - item.getDurability();
		int maxDurability = item.getType().getMaxDurability();
		ChatColor durabilityIndicator = getDurabilityColor(maxDurability, currentDurability);
		String durability = durabilityIndicator + "" + currentDurability + ChatColor.GRAY + "/" + ChatColor.WHITE + item.getType().getMaxDurability();
		return durability;
	}

	private ChatColor getDurabilityColor(int maxDurability, int currentDurability) {
		ChatColor durabilityIndicator;
		double percentageOfDurability = ((double)currentDurability / (double)item.getType().getMaxDurability()) * (double)100;
		if (percentageOfDurability >= 75) {
			durabilityIndicator = ChatColor.GREEN;
		} else if (percentageOfDurability >= 50 && percentageOfDurability < 75) {
			durabilityIndicator = ChatColor.YELLOW;
		} else if (percentageOfDurability >= 25 && percentageOfDurability < 50) {
			durabilityIndicator = ChatColor.RED;
		} else {
			durabilityIndicator = ChatColor.DARK_RED;
		}
		return durabilityIndicator;
	}
}
