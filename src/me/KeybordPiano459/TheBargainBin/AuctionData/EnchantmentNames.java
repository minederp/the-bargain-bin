package me.KeybordPiano459.TheBargainBin.AuctionData;

public enum EnchantmentNames {
    PROTECTION_ENVIRONMENTAL("PROTECTION"),
    PROTECTION_FIRE("FIRE_PROTECTION"),
    PROTECTION_FALL("FEATHER_FALLING"),
    PROTECTION_EXPLOSIVE("BLAST_PROTECTION"),
    PROTECTION_PROJECTILE("PROJECTILE_PROTECTION"),
    OXYGEN("RESPIRATION"),
    WATER_WORKER("AQUA_AFFINITY"),
    THORNS("THORNS"),
    DAMAGE_ALL("SHARPNESS"),
    DAMAGE_UNDEAD("SMITE"),
    DAMAGE_ARTHROPODS("BANE_OF_ARTHROPODS"),
    KNOCKBACK("KNOCKBACK"),
    FIRE_ASPECT("FIRE_ASPECT"),
    LOOT_BONUS_MOBS("LOOTING"),
    DIG_SPEED("EFFICIENCY"),
    SILK_TOUCH("SILK_TOUCH"),
    DURABILITY("UNBREAKING"),
    LOOT_BONUS_BLOCKS("FORTUNE"),
    ARROW_DAMAGE("POWER"),
    ARROW_KNOCKBACK("PUNCH"),
    ARROW_FIRE("FLAME"),
    ARROW_INFINITE("INFINITY"),
	PROTECTION_EXPLOSIONS("BLAST_PROTECTION");

    private static String realname = "";

    EnchantmentNames(String name) {
    }

    public static String getName(String ename) {
        EnchantmentNames enchant = EnchantmentNames.valueOf(ename);
        realname = enchant.toString();
        return realname;
    }
}