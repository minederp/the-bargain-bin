package me.KeybordPiano459.TheBargainBin.AuctionData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import sun.reflect.generics.tree.BaseType;

public class Auction {

	public Auction(Player player, int amount, AuctionItem item) {
		this.player = player.getName();
		this.amount = amount;
		this.item = item;
	}
	private String bidder;
	private String player;
	private int amount;
	private AuctionItem item;
	private int time;
	private boolean bidon;

	public int getAmount() {
		return this.amount;
	}

	public Player getBidder() {
		if (bidder != null) {
			return Bukkit.getServer().getPlayer(this.bidder);
		}
		return null;
	}

	public ItemStack getItemStack() {
		return this.getItem().getItem();
	}

	public AuctionItem getItem() {
		return this.item;
	}

	public Player getAuctionStarter() {
		Player onlinePlayer = null;
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			if (player.getName().equalsIgnoreCase(this.player)) {
				onlinePlayer = player;
			}
		}
		if (onlinePlayer == null) {
			onlinePlayer = Bukkit.getServer().getOfflinePlayer(this.player).getPlayer();
		}
		return onlinePlayer;
	}

	public int getTime() {
		return this.time;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setBidder(Player player) {
		this.bidder = player.getName();
	}

	public void setTime(int seconds) {
		this.time = seconds;
	}

	public void setWasBidOn(boolean bidon) {
		this.bidon = bidon;
	}

	public boolean wasBidOn() {
		return this.bidon;
	}

	public String[] getAuctionInfo() {
		List<String> linesToSend = new ArrayList<>();
		linesToSend.add(ChatColor.RED + getAuctionStarter().getName() + " is selling:");
		linesToSend.add(ChatColor.GRAY + "    Cost: " + ChatColor.WHITE + "$" + getAmount());
		linesToSend.add(ChatColor.GRAY + "    Item: " + ChatColor.WHITE + getItem().getItemName());
		String extraInfo = getItem().getExtraInfo();
		if(extraInfo != null){
			linesToSend.add(ChatColor.GRAY + "    ExtraInfo: " + ChatColor.WHITE + getItem().getExtraInfo());
		}
		if (getItem().hasCustomName()) {
			linesToSend.add(ChatColor.GRAY + "    CustomName: " + ChatColor.WHITE + getItem().getCustomName());
		}
		if (getItem().hasLore()) {
			int lineNum = 1;
			for (String line : getItem().getLore()) {
				linesToSend.add(ChatColor.GRAY + "    Lore: " + line + " #" + lineNum);
				lineNum++;
			}
		}
		linesToSend.add(ChatColor.GRAY + "    Quantity: " + ChatColor.WHITE + getItem().getItem().getAmount());
		if(getItem().getBaseType()!= null && getItem().getBaseType().equals(BaseItemType.TOOL)){
			linesToSend.add(ChatColor.GRAY + "    Durability: " + ChatColor.WHITE + getItem().getDurabiltyInfo());
		}
		if(getItem().hasEnchants()){
			linesToSend.add(ChatColor.GRAY + "    Enchantments: " + ChatColor.WHITE + parseEnchantments(getItem().getEnchantments()));
		}
		return linesToSend.toArray(new String[0]);
	}
	
	private String parseEnchantments(Map<Enchantment, Integer> enchantments){
		String enchantmentstr = "";
		for (Map.Entry<Enchantment, Integer> ent : enchantments.entrySet()) {
			String enchantName = EnchantmentNames.getName(ent.getKey().getName());
			enchantmentstr += EnchantmentNames.getName(enchantName).substring(0, 1).toUpperCase() + ent.getKey().getName().toString().substring(1).toLowerCase().replaceAll("_", " ") + " " 
					+ Numerals.convertIntegerToRoman(ent.getValue()) + ", ";
		}
		return enchantmentstr;
	}
}