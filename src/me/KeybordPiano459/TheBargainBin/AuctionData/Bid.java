package me.KeybordPiano459.TheBargainBin.AuctionData;

import me.KeybordPiano459.TheBargainBin.AuctionData.Auction;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Bid {

    public Bid(Auction auction, Player player, int amount) {
        this.auction = auction;
        this.player = player.getName();
        this.amount = amount;
    }

    private Auction auction;
    private String player;
    private int amount;

    public Auction getAuction() {
        return this.auction;
    }

    public Player getPlayer() {
        return Bukkit.getServer().getPlayer(this.player);
    }

    public int getAmount() {
        return this.amount;
    }
}
