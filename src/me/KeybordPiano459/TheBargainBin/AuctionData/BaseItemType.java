package me.KeybordPiano459.TheBargainBin.AuctionData;

import org.apache.commons.lang.StringUtils;
import org.bukkit.SkullType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public enum BaseItemType {

	TOOL,
	SKULL(397),
	WOOL(35),
	POTION(373),
	BOOK(403),
	DYE(351);
	
	private int itemID = -1;

	private BaseItemType(int itemID) {
		this.itemID = itemID;
	}

	private BaseItemType() {
	}

	public int getItemID() {
		return itemID;
	}

	public static BaseItemType getBaseItemType(ItemStack item) {
		for (BaseItemType type : BaseItemType.values()) {
			if (type.getItemID() != -1 && type.getItemID() == item.getTypeId()) {
				return type;
			}
		}
		if (item.getType().getMaxDurability() > 0) {
			return TOOL;
		}
		return null;
	}
	
	public static String getExtendInfo(AuctionItem item){
		String extraInfo = null;
		if(item.getBaseType() != null){
			switch(item.getBaseType()){
				case WOOL:
				case DYE:
					String temp = item.getItem().getData().toString();
					extraInfo = temp.split("[(]")[0];
					break;
				case SKULL:
					SkullType skullType = determineSkullType(item.getItem().getData().getData());
					if(!skullType.equals(SkullType.PLAYER)){
						extraInfo = StringUtils.capitalize(skullType.toString().replace("_", " ").toLowerCase());
					} else {
						SkullMeta meta = (SkullMeta) item.getItem().getItemMeta();
						extraInfo = meta.getOwner();
					}
					break;
				case BOOK:
				case POTION:
					//fuckmethissucks;
					break;
				case TOOL:
	//				extraInfo = "CUSTOMNAME: "+item.getItem().getItemMeta().getDisplayName();
				default:
					break;
			}
			return extraInfo;
		}
		return null;
	}

	private static SkullType determineSkullType(byte data) {
		switch (data) {
			case 0:
				return SkullType.SKELETON;
			case 1:
				return SkullType.WITHER;
			case 2:
				return SkullType.ZOMBIE;
			case 3:
				return SkullType.PLAYER;
//						return ((SkullMeta) item.getItemMeta()).getOwner() + "'s head";
			case 4:
				return SkullType.CREEPER;
			default:
				return null;
		}
	}
}